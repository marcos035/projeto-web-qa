const { faker } = require('@faker-js/faker');
const cpfName  = require('./utils/npm_test');

const{login,I}= inject();

const randomName = faker.name.fullName(); 
const randomEmail = faker.internet.email(); 
const randomPassword= faker.internet.password();
const randomLastName= faker.name.lastName();
const randomcompany= faker.company.name();
const randomAddress= faker.address.streetAddress();
const randomSecondaryAddress= faker.address.secondaryAddress();
const randomState= faker.address.state();
const randomZipCode=faker.address.zipCode()
const randomCity=faker.address.city()
const randomNumber=faker.phone.number()


Feature('login');

Scenario('LOGIN COM SUCESSO',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/');
    I.click('Login')
    I.waitForText('Login', 10)

    I.fillField('#user', 'marcos123@123.com')
    I.click('#password')
    I.fillField('#password' , secret('1232434554'));
    I.click('#btnLogin')
    I.waitForText('Login realizado')
}).tag('@sucesso')

Scenario('Tentando Logar digitando apenas o e-mail',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/');
    I.click('Login')
    I.waitForText('Login', 10)
    I.fillField('#user', 'marcos123@gmail.com')
    I.click('#btnLogin')
    I.waitForText('Senha inválida.')
    
}).tag('@onlyEmail')


Scenario('Tentando logar sem digitar e-mail e senha',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/');
    I.click('Login')
    I.waitForText('Login', 10)
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.')

}).tag('@null')

Scenario('Tentando Logar digitando apenas a senha',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/');
    I.click('Login')
    I.waitForText('Login', 10)
    I.click('#password')
    I.fillField('#password', secret('121212121212'))
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.')
}).tag('@onlyPassword')

BeforeSuite(()=>{
    console.log(cpfName.cpfName())

})
// Before( ()=>{
//     I.amOnPage('https://automationexercise.com/');
// })

After(()=>{
console.log('depois')
})

AfterSuite(()=>{
    console.log('Depois de tudo')
})

Scenario('Criando Cadastro com sucesso',  ({ I }) => {
    I.amOnPage('https://automationexercise.com/');
    I.click('Signup / Login')
    I.fillField('//*[@id="form"]/div/div/div[3]/div/form/input[2]', randomName)
    I.fillField('//*[@id="form"]/div/div/div[3]/div/form/input[3]',randomEmail)
    I.click('Signup')
    I.click('#id_gender1')
    I.fillField('#password', randomPassword)

    I.scrollTo('#days')

    I.selectOption('#days', '19')
    I.selectOption('#months', '1')
    I.selectOption('#years','1999')

    I.waitForText('ADDRESS INFORMATION',10)

    I.fillField('#first_name', randomName)
    I.fillField('#last_name', randomLastName)
    I.fillField('#company', randomcompany)

    I.scrollTo('#address1')

    I.fillField('#address1', randomAddress)
    I.fillField('#address2', randomSecondaryAddress)

    I.selectOption('#country', 'New Zealand')
    I.fillField('#state', randomState)
    I.fillField('#city',randomCity)
    I.fillField('#zipcode',randomZipCode)
    I.fillField('#mobile_number',randomNumber)

    I.click('Create Account')

    I.waitForText('Continue',10)

   
}).tag('@creatAccount')

Scenario('Login with success',({I})=>{
          I.amOnPage('https://automationexercise.com/');
          I.click('Signup / Login')
          I.fillField('//*[@id="form"]/div/div/div[3]/div/form/input[2]', 'Marcos Fideles')
          I.fillField('//*[@id="form"]/div/div/div[3]/div/form/input[3]','marcos1234@gmail.com')
          I.click('Signup')

}).tag('@with')