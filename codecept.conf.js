const { setHeadlessWhen, setCommonPlugins } = require('@codeceptjs/configure');
const server = require('./server/server');
// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();

/** @type {CodeceptJS.MainConfig} */
exports.config = {
  tests: './*_test.js',
  output: './output',
  helpers: {
    Playwright: {
      url: 'http://localhost',
      show: false,
      browser: 'chromium'
    }
  },
  include: {
    I: './steps_file.js'
  },
  bootstrap: async ()=>{
    await server.start();
    }, 
    teardown: async ()=>{
    await server.stop();
    }, 
  name: 'projeto 2',

  plugins:{
    mocha: {
      reporterOptions: {
          reportDir: "output"
      }
    },
    allure: {
      enabled: true
    },
    retryFailedStep:{
     enabled: true
    },
    stepByStepReport: {
      enabled: false,
      deleteSuccessful: false,
      screenshotsForAllureReport: true,
      fullPageScreenshots:true
    },
    autoLogin: {
      enabled: true,
      saveToFile: true,
      inject: 'login',
      users: {
        user: {
          // loginAdmin function is defined in `steps_file.js`
          login: (I) => {
          I.amOnPage('/')
          I.fillField('//*[@id="form"]/div/div/div[3]/div/form/input[2]', 'Marcos Fideles')
          I.fillField('//*[@id="form"]/div/div/div[3]/div/form/input[3]','marcos1234@gmail.com')
          I.click('Signup')
          },
          check: (I) => {
             I.amOnPage('/')
             I.see('Marcos Fideles')
          }
        }
      }
    }
    
    
    
    }
}